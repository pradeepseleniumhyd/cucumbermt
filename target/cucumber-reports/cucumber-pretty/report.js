$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/Lenovo/Documents/Meghana/CucumberMercuryTours/src/main/resources/features/HomePage.feature");
formatter.feature({
  "line": 1,
  "name": "Validation of Mercury Tours Home Page",
  "description": "",
  "id": "validation-of-mercury-tours-home-page",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "Home Page Validation of Mercury Tours",
  "description": "",
  "id": "validation-of-mercury-tours-home-page;home-page-validation-of-mercury-tours",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "Open the browser",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Enter the url \"http://newtours.demoaut.com/\"",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "homepage is displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Enter the username",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Enter the password",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Signin is clicked",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageStepDefinition.open_the_browser()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({
  "location": "HomePageStepDefinition.homepage_is_displayed()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "HomePageStepDefinition.enter_the_username()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "HomePageStepDefinition.enter_the_password()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "HomePageStepDefinition.signin_is_clicked()"
});
formatter.result({
  "status": "skipped"
});
});