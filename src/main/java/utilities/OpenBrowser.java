package utilities;


import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class OpenBrowser
{
	String servername = "local";
	
	public WebDriver getBrowser(WebDriver driver,String browsername) throws Exception
	{

		if(browsername.equalsIgnoreCase("chrome"))
		{
			if(servername.equalsIgnoreCase("remote"))
			{
				DesiredCapabilities desiredCap = DesiredCapabilities.chrome();
				desiredCap.setPlatform(Platform.WINDOWS);
				desiredCap.setBrowserName("chrome");
				driver = new RemoteWebDriver(new URL("http://192.168.29.122:4444/wd/hub"), desiredCap);
			}else
			{
			
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\Drivers\\chromedriver.exe");
			 driver = new ChromeDriver();
			}
		
		}else if(browsername.equalsIgnoreCase("firefox"))
		{
			if(servername.equalsIgnoreCase("remote"))
			{
				DesiredCapabilities desiredCap = DesiredCapabilities.chrome();
				desiredCap.setPlatform(Platform.WINDOWS);
				desiredCap.setBrowserName("ff");
				driver = new RemoteWebDriver(new URL("http://192.168.29.122:4444/wd/hub"), desiredCap);
			}else
			{
			
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"\\Drivers\\geckodriver.exe");
			
		//	FirefoxDriver driver = new Firefo
			}

		}else if(browsername.equalsIgnoreCase("ie"))
		{
			
			if(servername.equalsIgnoreCase("remote"))
			{
				DesiredCapabilities desiredCap = DesiredCapabilities.chrome();
				desiredCap.setPlatform(Platform.WINDOWS);
				desiredCap.setBrowserName("ie");
				driver = new RemoteWebDriver(new URL("http://192.168.29.122:4444/wd/hub"), desiredCap);
			}else
			{
			
			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"\\Drivers\\IEDriverServer.exe");
			 driver = new InternetExplorerDriver();
			}

		}else
		{

		}
		
		
		return driver;
		

	}
	
	
	
	
	

}
