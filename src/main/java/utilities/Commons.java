package utilities;


import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Commons 
{
	public WebDriver driver;
	public XSSFWorkbook workbook;
	public XSSFSheet sheet;
	public static String ExcelSheetPath = System.getProperty("user.dir")+"/TestData/YourLogo.xlsx";
	public static String configPropFilePath = System.getProperty("user.dir") + "/Resources/config.properties";
	public static String orPropFilePath = System.getProperty("user.dir") + "/Resources/OR.properties";

	public static String reportPath = System.getProperty("user.dir") + "/Reports/YourLogoReport.html";

	public FileInputStream fis;
	public Properties config,or;
	public static ExtentReports extent;
	public ExtentTest test;
	
	/*public String Login_usernameData = "pradeepseleniumhyd@gmail.com" ;
	public String Login_PasswordData = "pradeepseleniumhyd" ;

	
	*/
	public void navigateURL(WebDriver driver,String url)
	{
		try
		{
		driver.get(url);
		Reporter.log(url+" is entered");
		takeScreenshot(driver, "URL");
		
		}catch(Exception e)
		{
			System.out.println(url + " is not entered : " + e.getMessage());

		}
	}
	
	
	public void validateTitle(WebDriver driver,String expectedTitle)
	{
		try
		{
			String actualTitle = driver.getTitle();
			System.out.println(actualTitle);
			
			if(expectedTitle.equals(actualTitle))
			{
				Reporter.log(actualTitle + " is displayed");
				//test.log(LogStatus.PASS,actualTitle + " is displayed");

			}else
			{
				Reporter.log(actualTitle + " is not displayed");
				//test.log(LogStatus.FAIL,actualTitle + " is not displayed");

			}
			
			
		}catch(Exception e)
		{
			System.out.println("Title is not matched : " + e.getMessage());
			Reporter.log("Title is not matched : " + e.getMessage());
			//test.log(LogStatus.FAIL, "Title is not matched : " + e.getMessage());

		}
		
	}
	
	
	
	public void enterData(WebDriver driver,String xpathvalue,String enterValue)
	{
		try
		{
			driver.findElement(By.xpath(xpathvalue)).click();
			driver.findElement(By.xpath(xpathvalue)).clear();
			driver.findElement(By.xpath(xpathvalue)).sendKeys(enterValue);
				//test.log(LogStatus.PASS, enterValue + " is entered");
			
		}catch(Exception e)
		{
			System.out.println("Enter Data method is not working : " + e.getMessage());
			Reporter.log("Enter Data method is not working : " + e.getMessage());
			//test.log(LogStatus.FAIL, "Enter Data method is not working : " + e.getMessage());

		}
		
	}
	
	
	public void click(WebDriver driver,String xpathvalue, String Title)
	{
		try
		{
			takeScreenshot(driver, Title);
			driver.findElement(By.xpath(xpathvalue)).click();
			Reporter.log(Title + " is clicked");
			//test.log(LogStatus.PASS,Title + " is clicked" + test.addScreenCapture(System.getProperty("user.dir")+"/Screenshots/"+Title+ ".jpg"));

		}catch(Exception e)
		{
			Reporter.log(Title + " is not clicked");
			System.out.println(Title + "is not clicked : " + e.getStackTrace());
			//test.log(LogStatus.FAIL,Title + "is not clicked : " + e.getStackTrace());


		}
	}
	
	public void validateTextByUsingEqualsIgnoreCase(WebDriver driver,String xpathvalue,String expectedvalue)
	{
		try
		{
			String actualvalue=driver.findElement(By.xpath(xpathvalue)).getText();
			if(expectedvalue.equalsIgnoreCase(actualvalue))
			{
				Reporter.log(actualvalue + " is displayed");
				//test.log(LogStatus.PASS,actualvalue + " is displayed");

			}else
			{
				Reporter.log(actualvalue + " is not displayed");
				//test.log(LogStatus.FAIL,actualvalue + " is not displayed");
			}
		}catch(Exception e)
		{
			Reporter.log(expectedvalue + " is not displayed " + e.getMessage());
			System.out.println(expectedvalue + " is not displayed " + e.getMessage());
			//test.log(LogStatus.FAIL,expectedvalue + " is not displayed " + e.getMessage());

		}
	}
	
	public void validateTextByUsingEquals(WebDriver driver,String xpathvalue,String expectedvalue)
	{
		try
		{
			String actualvalue=driver.findElement(By.xpath(xpathvalue)).getText();
			if(expectedvalue.equals(actualvalue))
			{
				Reporter.log(actualvalue + " is displayed");
				//test.log(LogStatus.PASS,actualvalue + " is displayed");

			}else
			{
				Reporter.log(actualvalue + " is not displayed");
				//test.log(LogStatus.FAIL,actualvalue + " is not displayed");

			}
		}catch(Exception e)
		{
			Reporter.log(expectedvalue + " is not displayed " + e.getMessage());
			System.out.println(expectedvalue + " is not displayed " + e.getMessage());
			//test.log(LogStatus.FAIL,expectedvalue + " is not displayed " + e.getMessage());

		}
	}
	
	
	public void validateTextByUsingContains(WebDriver driver,String xpathvalue,String expectedvalue)
	{
		try
		{
			String actualvalue=driver.findElement(By.xpath(xpathvalue)).getText();
			if(actualvalue.contains(expectedvalue))
			{
				Reporter.log(actualvalue + " is displayed");
				//test.log(LogStatus.PASS,actualvalue + " is displayed");

			}else
			{
				Reporter.log(actualvalue + " is displayed");
				//test.log(LogStatus.FAIL,actualvalue + " is not displayed");

			}
		}catch(Exception e)
		{
			Reporter.log(expectedvalue + " is not displayed " + e.getMessage());
			System.out.println(expectedvalue + " is not displayed " + e.getMessage());
			//test.log(LogStatus.FAIL,expectedvalue + " is not displayed " + e.getMessage());

		}
	}
	
	public XSSFSheet excelReading() throws Exception
	{
		try
		{
			 workbook = new XSSFWorkbook(ExcelSheetPath);
			 sheet = workbook.getSheet("TestCases");
			System.out.println(sheet.getLastRowNum());		
			return sheet;
		}catch(Exception e)
		{
			System.out.println("Excel Reading is not working : " + e.getMessage());
			return null;
		}
	}

	//Configuration code
	public Properties configProperties() throws Exception
	{
		 fis = new FileInputStream(configPropFilePath);
		 config = new Properties();
		 config.load(fis);
		 return config;
	}
	
	
	//Configuration code
		public Properties orProperties() throws Exception
		{
			 fis = new FileInputStream(orPropFilePath);
			 or = new Properties();
			 or.load(fis);
			 return or;
		}
		
		
		
		public static void takeScreenshot(WebDriver driver, String filename) throws Exception
		{
			File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(screenshot, new File(System.getProperty("user.dir")+"/Screenshots/"+filename+".jpg"));
			
	
			
		}
		
}
