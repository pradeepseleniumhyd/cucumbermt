package testRunner;

import org.junit.runner.RunWith;
import org.testng.annotations.Test;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="C:\\Users\\Lenovo\\Documents\\Meghana\\CucumberMercuryTours\\src\\main\\resources\\features\\HomePage.feature",
glue={"stepDefinitions"},
dryRun=true,
plugin = { "pretty", "html:target/cucumber-reports/cucumber-pretty",
		"json:target/cucumber-reports/CucumberTestReport.json", "rerun:target/cucumber-reports/rerun.txt" })
public class Runner {
	
	

}
