package stepDefinitions;

import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utilities.Commons;
import utilities.OpenBrowser;

public class HomePageStepDefinition extends Commons
{
	WebDriver driver;
	OpenBrowser openbrowser = new OpenBrowser();
	
	
	@Given("^Open the browser$")
	public void open_the_browser() throws Throwable {
		System.out.println("Open the browser");
		driver = openbrowser.getBrowser(driver, "chrome");
	}

	@When("^Enter the url$")
	public void enter_the_url() throws Throwable {
		System.out.println("Enter the url");
		navigateURL(driver, "http://newtours.demoaut.com/");
	}
	
	
	@When("^Enter the url \"([^\"]*)\"$")
	public void enter_the_url(String url) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		navigateURL(driver,url);

	}

	

	@Then("^homepage is displayed$")
	public void homepage_is_displayed() throws Throwable {
		System.out.println("Home Page is displayed");
		validateTitle(driver, "Welcome: Mercury Tours");
	}

	@Then("^Enter the username$")
	public void enter_the_username() throws Throwable {
		System.out.println("Enter the username");
		enterData(driver, "//input[@name='userName']", "Meghana");

	}

	@Then("^Enter the password$")
	public void enter_the_password() throws Throwable {
		System.out.println("Enter the password");
		enterData(driver, "//input[@name='password']", "Meghana123");

	}

	@Then("^Signin is clicked$")
	public void signin_is_clicked() throws Throwable {
		System.out.println("Sign in is clicked");
		
	}


}
